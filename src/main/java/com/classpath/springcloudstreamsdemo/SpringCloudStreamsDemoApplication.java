package com.classpath.springcloudstreamsdemo;

import com.classpath.springcloudstreamsdemo.channel.InputChannels;
import com.classpath.springcloudstreamsdemo.channel.OutputChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;

@SpringBootApplication
@EnableBinding({InputChannels.class, OutputChannels.class})
public class SpringCloudStreamsDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringCloudStreamsDemoApplication.class, args);
    }
}
