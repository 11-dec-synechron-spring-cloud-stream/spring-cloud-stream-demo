package com.classpath.springcloudstreamsdemo.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface InputChannels {

    @Input("shipmentChannel")
    SubscribableChannel orderInputChannel();
}