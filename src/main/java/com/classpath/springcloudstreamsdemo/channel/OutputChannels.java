package com.classpath.springcloudstreamsdemo.channel;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.integration.annotation.Router;
import org.springframework.messaging.MessageChannel;

public interface OutputChannels {

    @Output("orderoutputchannel")
    MessageChannel orderOutputChannel();

    @Output("shipmentoutputchannel")
    MessageChannel shipmentOutputChannel();
}