package com.classpath.springcloudstreamsdemo.config;

import com.github.javafaker.Faker;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @ConditionalOnProperty(name = "loadfaker", prefix = "app", havingValue = "true", matchIfMissing = true)
    @Bean
    public Faker faker(){
        return new Faker();
    }
}