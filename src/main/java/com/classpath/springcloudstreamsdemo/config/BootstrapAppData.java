package com.classpath.springcloudstreamsdemo.config;

import com.classpath.springcloudstreamsdemo.channel.OutputChannels;
import com.classpath.springcloudstreamsdemo.model.Product;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;
import java.util.function.Supplier;
import static java.lang.Thread.sleep;
import static java.util.stream.IntStream.range;

@Component
@Slf4j
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final Faker faker;
    //private final Source source;
    private final OutputChannels outputChannels;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
      log.info("==================== Application is now ready ===========================");
      range(1, 100).forEach(index ->  {
          final Product product = generateProduct().get();
          //this.source.output().send(MessageBuilder.withPayload(product).build());
          this.outputChannels.orderOutputChannel().send(MessageBuilder.withPayload(product).build());
          this.outputChannels.shipmentOutputChannel().send(MessageBuilder.withPayload(product).build());
          try {
              sleep(1000);
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
          System.out.println(product);

      });
    }

    /*
       Function returning a Supplier
     */
    private Supplier<Product> generateProduct(){
        return  () -> Product.builder()
                        .id(faker.number().randomNumber())
                        .price(faker.number().randomDouble(2, 25000, 50000))
                        .name(faker.commerce().productName())
                    .build();
    }
}