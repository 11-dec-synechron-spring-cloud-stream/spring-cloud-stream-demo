package com.classpath.springcloudstreamsdemo.config;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Calculator {
    public int sum(int a, int b) {
        return a + b;
    }

    public int sub(int a, int b) {
        return a - b;
    }

    public double sciOperator(BiFunction<Integer, Integer, Double> sciFun, int val1, int val2){
        return sciFun.apply(val1, val2);
    }

    public static double trignometricFun(Function<Double, Double> trignometricFuc, double operand){
        return trignometricFuc.apply(operand);
    }

    public static void main(String[] args) {
        Function<Double, Double> tanOperator = (value) -> Math.tan(value);
        trignometricFun(tanOperator, 34);

    }
}