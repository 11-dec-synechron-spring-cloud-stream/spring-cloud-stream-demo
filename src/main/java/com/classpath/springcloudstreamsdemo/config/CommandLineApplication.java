package com.classpath.springcloudstreamsdemo.config;

import com.classpath.springcloudstreamsdemo.model.Product;
import com.classpath.springcloudstreamsdemo.model.ProductType;
import com.classpath.springcloudstreamsdemo.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import java.util.List;
import java.util.function.*;

import static java.util.Arrays.asList;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class CommandLineApplication implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    private final ProductRepository productRepository;

    private static BiFunction<Integer, Integer, Integer> add = (operand1, operand2) -> operand1 + operand2;
    private static BiFunction<Integer, Integer, Integer> sub = (operand1, operand2) -> operand1 - operand2;
    private static BiFunction<Integer, Integer, Integer> product = (operand1, operand2) -> operand1 - operand2;
    private static BiFunction<Integer, Integer, Integer> mod = (operand1, operand2) -> operand1 % operand2;
    private static BiFunction<Integer, Integer, Integer> divide = (operand1, operand2) -> operand1 / operand2;
    @Override
    public void run(String... args) throws Exception {
        final String[] beanDefinitionNames = applicationContext.getBeanDefinitionNames();
        //No for loops
/*
        for(String bean: beanDefinitionNames){
            log.info("Bean name: {} ", bean);
        }
*/
        //
        //asList(beanDefinitionNames).forEach(bean -> log.info("Bean name: {} ", bean));

/*        Predicate<Product> productLessThan25K = new Predicate<Product>() {
            @Override
            public boolean test(Product product) {
                return product.getPrice() < 25000;
            }
        };*/

        Predicate<Product> productLessThan25K = product -> product.getPrice() < 25000;
        //Predicate<Product> productGtThan25K = product -> product.getPrice() > 25000;
        Predicate<Product> productGtThan25K =productLessThan25K.negate();

        Predicate<Product> isIPhone = product -> product.getName().equalsIgnoreCase("iphone-12");
        Predicate<Product> isMacBookPro = product -> product.getName().equalsIgnoreCase("macbook-pro");

        Predicate<Product> isApple = isIPhone.or(isMacBookPro);
        Predicate<Product> isSamsung = product -> product.getProductType().equals(ProductType.SAMSUNG);
        Predicate<Product> isNotApple = isApple.negate();

        Product product1 = Product.builder().id(12).price(24000).name("iPhone-12").productType(ProductType.APPLE).build();

        Consumer<Product> printProduct = System.out::println;

        log.info(" Is this product IPhone ::  {}", isIPhone.test(product1));

        final List<Product> products = asList(
                Product.builder().id(12).price(24000).name("iPhone-12").productType(ProductType.APPLE).build(),
                Product.builder().id(13).price(34000).name("Samsung-S12").productType(ProductType.SAMSUNG).build(),
                Product.builder().id(14).price(54000).name("OPPO").productType(ProductType.OPPO).build(),
                Product.builder().id(15).price(74000).name("Macbook-Pro").productType(ProductType.APPLE).build()
        );
        //Product p = new Product(11,22, "one", "two", false, true , true);
        //streams processing
        //products.stream().filter(isApple.or(isSamsung)).forEach(printProduct);

        /*
            Functional descriptors
               Predicate<T> -> accept an input, returns boolean
               Consumer<T> -> accept an input, returns void
               Function<T, R> -> accept an input , return an output
               BiFunction<T,U,R> -> accepts two inputs and returns a third output
               Supplier<T> -> no input, return t
         */
        //Function<Product, String> mapProductToName = product -> product.getName();
        Function<Product, String> mapProductToName = product -> product.getName().toUpperCase();

        Function<Double, Double> mapINRtoUSD = priceInINR -> priceInINR / 75;
        //Function<Product, Double> mapProductToPrice = product -> product.getPrice();

        //Function<Product, Double> mapProductToPrice = product -> mapINRtoUSD.apply(product.getPrice());
        Function<Product, Double> mapProductToPrice = product -> {
            double priceInINR = product.getPrice();
            return priceInINR / 75;
        };

        BiFunction<Integer, Integer, String> sum = (operand1, operand2) -> "The sum of two numbers is "+ (operand1 + operand2);

        BiFunction<Integer, Integer, Integer> add = (operand1, operand2) -> operand1 + operand2;
        BiFunction<Integer, Integer, Integer> sub = (operand1, operand2) -> operand1 - operand2;
        BiFunction<Integer, Integer, Integer> product = (operand1, operand2) -> operand1 - operand2;
        BiFunction<Integer, Integer, Integer> mod = (operand1, operand2) -> operand1 % operand2;
        BiFunction<Integer, Integer, Integer> divide = (operand1, operand2) -> operand1 / operand2;


        //Function<Product, ProductType> mapProductToProductType = product -> product.getProductType();
        //Method references
        Function<Product, ProductType> mapProductToProductType = Product::getProductType;

        final String nameOfProduct = mapProductToName.apply(product1);
        final double priceOfProduct = mapProductToPrice.apply(product1);
//        log.info("Product name : {}", nameOfProduct);
//        log.info("Product price : {}", priceOfProduct);

        products
                .stream()
                .filter(isApple)
                .map(mapProductToPrice)
                .limit(1)
                .forEach(priceInUSD -> System.out.println(" Price in USD :: "+ priceInUSD));

        final int result = calculator(add, 34, 56);
        //System.out.println("The result is "+ result);

        Supplier<Product> supplier = () -> Product.builder().id(12).price(24000).name("iPhone-12").productType(ProductType.APPLE).build();
    }

    public static int calculator(BiFunction<Integer, Integer, Integer> biFunction, int val1, int val2){
        return biFunction.apply(val1, val2);
    }
    public  Product fetchProductById(long productId){
        return  this.productRepository
                .findProductById(productId)
                //.orElseThrow( () -> new IllegalArgumentException("Invalid product Id"));
                .orElseGet(() -> Product.builder().id(1111).name("default").price(1111).build());
    }
}