package com.classpath.springcloudstreamsdemo.controller;

import com.classpath.springcloudstreamsdemo.model.Product;
import com.classpath.springcloudstreamsdemo.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/api/products")
@RequiredArgsConstructor
public class ProductRestController {

    private final ProductService productService;

    @GetMapping
    public Set<Product> fetchProducts(){
        return this.productService.fetchProducts();
    }

    @PostMapping
    public Product saveProduct(@RequestBody Product product){
        return this.productService.saveProduct(product);
    }
}