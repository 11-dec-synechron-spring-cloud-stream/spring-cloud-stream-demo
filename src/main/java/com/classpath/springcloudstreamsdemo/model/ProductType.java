package com.classpath.springcloudstreamsdemo.model;

public enum ProductType {
    APPLE,
    SAMSUNG,
    OPPO,
    VIVO
}
