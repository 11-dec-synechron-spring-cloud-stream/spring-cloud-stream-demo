package com.classpath.springcloudstreamsdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Shipment {
    private boolean status;
    private String city;
    private String zipCode;
}