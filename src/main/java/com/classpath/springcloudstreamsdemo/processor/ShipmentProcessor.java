package com.classpath.springcloudstreamsdemo.processor;

import com.classpath.springcloudstreamsdemo.model.Shipment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ShipmentProcessor {

    @StreamListener("shipmentChannel")
    public void processShipment(Shipment shipment){
        log.info(" Processing the shipment :: {} ", shipment);
    }
}