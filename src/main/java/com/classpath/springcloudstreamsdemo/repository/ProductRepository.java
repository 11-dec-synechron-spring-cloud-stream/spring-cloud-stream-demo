package com.classpath.springcloudstreamsdemo.repository;

import com.classpath.springcloudstreamsdemo.model.Product;
import com.classpath.springcloudstreamsdemo.model.ProductType;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class ProductRepository {

    public Optional<Product> findProductById(long productId){
        if (productId < 1000){
            return Optional.empty();
        }
        return Optional.of(Product.builder().id(12).price(24000).name("iPhone-12").productType(ProductType.APPLE).build());
    }
}