package com.classpath.springcloudstreamsdemo.service;

import com.classpath.springcloudstreamsdemo.channel.OutputChannels;
import com.classpath.springcloudstreamsdemo.model.Product;
import com.classpath.springcloudstreamsdemo.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;
//    private final Source source;

    private final OutputChannels outputChannels;

    public Set<Product> fetchProducts(){
        return new HashSet<>();
    }

    public Product saveProduct(Product product) {
        //saving the order in the repository
        //publish an event to the broker.
        log.info("Saved the product in the database successfully ::");
        //this.restTemplate.postForEntity(""http://ip:port"
        //this.source.output().send(MessageBuilder.withPayload(product).build());
        this.outputChannels.orderOutputChannel().send(MessageBuilder.withPayload(product).build());
        this.outputChannels.shipmentOutputChannel().send(MessageBuilder.withPayload(product).build());
        return product;
    }
}